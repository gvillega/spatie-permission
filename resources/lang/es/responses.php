<?php

return [
    'messages' => [
        'created' => ':model se creó satisfactoriamente.',
        'not_created' => 'No fue posible crear :model.',
        'not_updated' => 'No fue posible actualizar :model.',
        'not_deleted' => 'No fue posible eliminar :model, :model tiene :related asociados que deben ser removidos previamente.',
        'not_found' => ':model no existe.',
        'conflict' => ':model fue actualizado antes de su petición, recargue la página y vuelva a intentarlo.',
        'updated' => ':model se actualizó satisfactoriamente.',
        'deleted' => ':model fue eliminado satisfactoriamente.',
        'unlock' => ':model fue desbloqueado satisfactoriamente.',
        'not_allowed' => 'No tiene permitido realizar acciones sobre :model.',
        'activated' => ':model fue habilitado satisfactoriamente.',
        'deactivated' => ':model fue deshabilitado satisfactoriamente.',
        'authorized' => ':model fue autorizado satisfactoriamente.',
        'unauthorized' => ':model fue desaprobado satisfactoriamente.',
        'not_permission' => "No se puede ejecutar la acción solicitada debido a que no cuenta con el permiso ':permission'.",
        'email_sent' => "El e-mail fue enviado satisfactoriamente a los destinatarios especificados.",
        'already_exists' => ":model ya existe y no puede ser duplicado.",
        'sorted' => ":model fue reordenado satisfactoriamente.",
        'appointment_already_taken' => "La cita ya fue tomada por alguien mas.",
        'appointment_canceled' => "La cita ha sido cancelada con éxito.",
        'route_started' => "La ruta a la cita ha sido iniciada de forma correcta.",
        'route_canceled' => "La ruta a la cita ha sido cancelada.",
        'route_finished' => "La ruta ha sido finalizada con éxito.",
        'verification_finished' => "La verificación ha finalizado con éxito.",
        'invalid_appointment' => "No se pudo ejecutar la petición, la cita está cancelada.",
        'invalid_appointment_finished' => "No se pudo ejecutar la petición, la cita ya ha sido finalizada.",
        'appointment_not_about_start' => "La cita no está próxima a comenzar.",
        'invalid_route' => "No se pudo ejecutar la petición, la ruta está cancelada.",
        'invalid_route_finished' => "No se pudo ejecutar la petición, la ruta ya ha sido finalizada.",
        'invalid_verification_vehicle' => "No se pudo crear la verificación, la verificación del vehículo especificado ya existe.",
        'invalid_appointment_route_not_finished' => "No se pudo crear la verificación, la ruta aún no ha sido finalizada.",
        'connected' => "Usuario conectado con éxito.",
        'disconnected' => "Usuario desconectado con éxito.",
        'not_connected' => "No fue posible conectarse con el sistema.",
        'not_disconnected' => "No fue posible desconectarse del sistema.",
        'datarer_has_appointments_in_course' => "La solicitud no pudo ser procesada, debe finalizar las citas en curso.",
        'password_reseted' => "La contraseña ha sido restablecida y enviada por correo de forma satisfactoria.",
        'not_verified' => "Su cuenta aún no ha sido verificada.",
        'not_authorized' => "Su cuenta no está autorizada.",
        'invalid_request' => "Petición inválida. Algunos parámetros son incorrectos.",
        'settings_updated' => 'Ajustes guardados satisfactoriamente.',
        'upgrade_required' => 'Hay una nueva versión disponible de ' . config('app.name') . ' App, para continuar se debe instalar la nueva versión.'
    ],

    'pronouns' => [
        'user' => 'El usuario|Usuarios',
        'role' => 'El rol|Roles',
        'datarer' => 'El datarero|Datareros',
        'client' => 'El cliente|Clientes',
        'policy' => 'La póliza|Pólizas',
        'appointment' => 'La cita|Citas',
        'password' => 'La contraseña|Contraseñas',
        'tracking' => 'El rastreo|Rastreos',
        'verification' => 'La verificación|Verificaciones',
        'route' => 'La ruta|Rutas',
        'notification' => 'La notificación|Notificaciones',
        'location' => 'La ubicación|Ubicaciones',
        'report' => 'El reporte|Reportes',
        'device_token' => 'El token del dispositivo|Tokens de dispositivos',
        'setting' => 'El ajuste|Ajustes',
        'payment_order' => 'La orden de pago|Órdenes de pago',
        'account' => 'La cuenta|Cuentas',
    ]
];
