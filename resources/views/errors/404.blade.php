@extends('layouts.app')

@section('title')
    Not found
@endsection

@section('content')
<div class="box box--grey">
    <div class="error-page">
        <h2 class="headline text-yellow text-center" style="float: none;"> 404</h2>

        <div class="error-content text-center" style="margin-left: 0;">
            <h3><i class="fa fa-warning text-yellow"></i> Oops! Página no encontrada.</h3>

            <p>
                No pudimos encontrar la página que estas buscando.
                Mientras tanto, puedes regresar a la <a href="{{ url('/') }}">página de inicio</a>.
            </p>
        </div>
        <!-- /.error-content -->
    </div>
    <!-- /.error-page -->
</div>
@endsection


@section('scripts')
    @parent
    <script type="text/javascript">
        var a = 'gola';
    </script>
@endsection