@extends('layouts.app')

@section('title')
Acceso denegado
@endsection

@section('content')
<section class="content">
    <div class="error-page">
        <h2 class="headline text-yellow text-center" style="float: none;"> 403</h2>

        <div class="error-content text-center" style="margin-left: 0;">
            <h3><i class="fa fa-warning text-yellow"></i> Acceso denegado.</h3>

            <p>
                No tienes permiso para acceder al recurso solicitado.
                Mientras tanto, puedes regresar a la <a href="{{ url('/home') }}">página de inicio</a>.
            </p>
        </div>
        <!-- /.error-content -->
    </div>
    <!-- /.error-page -->
</section>
@endsection
