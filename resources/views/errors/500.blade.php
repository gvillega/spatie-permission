@extends('layouts.app')

@section('title')
Error interno del servidor
@endsection

@section('content')
<section class="content">

    <div class="error-page">
        <h2 class="headline text-yellow text-center" style="float: none;"> 500</h2>

        <div class="error-content text-center" style="margin-left: 0;">

            <p>
                Trabajaremos para solucionarlo de inmediato.
                Mientras tanto, puedes regresar a la <a href="{{ url('/') }}">página de inicio</a>.
            </p>
        </div>
    </div>
    <!-- /.error-page -->

</section>
@endsection
