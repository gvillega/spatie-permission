@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                   <table border="1">
                    <thead>
                        <th>Nombre</th>
                       <th></th>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td> {{$user->name}} </td>
                            <td><a href=" {{ route('user.edit', $user->id) }} " class="btn btn-primary">Edit</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
