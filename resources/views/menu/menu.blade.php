@php 

    $user = Auth::user();
    $permissions = [];
    if (empty(Session::get('permissions'))) {
        Session::put('permissions', $user->getPermission());
    }
    $permissions = Session::get('permissions');

    //dd($permissions);
@endphp

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Home</a></li>
      @if (hasPermission($permissions, 'usuarios'))
        <li><a href="{{ route('user.index') }}">usuarios</a></li>
      @endif
      @if (hasPermission($permissions, 'roles'))
        <li><a href="{{ route('roles.index') }}">roles</a></li>
      @endif
      <li><a href="#">Page 3</a></li>
    </ul>
  </div>
</nav>