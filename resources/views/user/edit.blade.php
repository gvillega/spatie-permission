@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <form action=" {{route('user.update', $user->id)}} " method="POST">
                        {{ method_field('PATCH') }}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="text" name="name"  value="{{ $user->name }}">
                        @foreach($permission as $permisso)
                            <div class="checkbox">
                                <label><input type="checkbox" value="{{ $permisso->id }}" name="permissions[]">{{ $permisso->name }}</label>
                            </div>
                        @endforeach


                        <button type="submit">Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection