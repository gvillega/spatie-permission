<?php


namespace App\Helpers;


use Illuminate\Support\Facades\Log;

class CountryCode
{
    CONST DEFAULT = "+52";

    public static function getCodes(){
        try {
            $path = resource_path()."/json/country_codes.json";
            $jsonString = file_get_contents($path);
            return json_decode($jsonString, true);
        }catch (\Exception $e){
            info("Error en: ".CountryCode::class);
            error_log($e->getMessage());
            return [];
        }
    }


}