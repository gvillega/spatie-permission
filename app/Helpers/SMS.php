<?php


namespace App\Helpers;

use Twilio\Rest\Client;

class SMS
{

    public static function send($number, $body){
        $account_sid =  config('services.twilio.sid');
        $auth_token = config('services.twilio.token');
        $twilio_number =  config('services.twilio.phone_number');
        try {
            $client = new Client($account_sid, $auth_token);
            return $client->messages->create("{$number}", [
                'from' => $twilio_number,
                'body' => $body
            ]);
        } catch (\Exception $e) {
            info($e->getMessage());
            return $e->getMessage();
        }

    }

}
