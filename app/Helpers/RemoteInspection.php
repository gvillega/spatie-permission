<?php


namespace App\Helpers;


use App\Models\Inspection;
use http\Client\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use Twilio\Rest\Client;

class RemoteInspection
{
    private $twilioAccountSid;
    private $twilioAccountToken;

    private $twilioApiKey;
    private $twilioApiSecret;

    public $twilio;

    private $room;


    public function __construct($old_credentials = false)
    {
        if ($old_credentials){
            $this->twilioAccountSid = config('services.twilio.sid_old');
            $this->twilioAccountToken = config('services.twilio.token_old');
            $this->twilioApiKey = config('services.twilio.key_old');
            $this->twilioApiSecret = config('services.twilio.secret_old');
        }else{
            $this->twilioAccountSid = config('services.twilio.sid');
            $this->twilioAccountToken = config('services.twilio.token');
            $this->twilioApiKey = config('services.twilio.key');
            $this->twilioApiSecret = config('services.twilio.secret');
        }

        try {
            $this->twilio = new Client($this->twilioAccountSid, $this->twilioAccountToken);
        } catch (ConfigurationException $e) {
            Log::info(" TWILIO ERROR : {$e->getMessage()}");
        }

        $this->room = null;
    }



    public function createIdentity(){
        $code = Str::random('8');
        return "remote-{$code}";
    }

    private function createRoomUnique(){
        $code = Str::random('8');
        return "room-{$code}";
    }

    public function createUniqueUrl(){
        $link = Str::random(10);
        $item = Inspection::whereLink($link)->first();
        if ($item){
            return $this->createUniqueUrl();
        }
        return $link;
    }


    public function createRoom(){

        $name = $this->createRoomUnique();
        $room = $this->twilio->video->v1->rooms->create([
            "uniqueName" => $name,
            "recordParticipantsOnConnect" => true
        ]);

        $this->room = $room;

        return $room->uniqueName;
    }

    public function getSID(){
        try {
            return $this->room->sid;
        }catch (\Exception $e){
            return null;
        }
    }

    public function finishRoom($room){
        try {
            $this->twilio->video->v1->rooms($room)->update("completed");
        }catch (\Exception $e){}
    }

    public function getToken($inspection, $remote = false){
        // A unique identifier for this user
        $identity = ($remote) ? $inspection->identity : auth()->user()->first_name;
        // Create access token, which we will serialize and send to the client
        $token = new AccessToken($this->twilioAccountSid, $this->twilioApiKey, $this->twilioApiSecret, 300,  $identity);
        // The specific Room we'll allow the user to access
        $roomName = ($inspection->room) ? $inspection->room :  $this->createRoom();
        // Create Video grant
        $videoGrant = new VideoGrant();
        $videoGrant->setRoom($inspection->room);
        // Add grant to token
        $token->addGrant($videoGrant);
        // render token to string
        return [
            "token" => $token->toJWT(),
            "identity" => $identity,
            "room" => $roomName
        ];
    }

    public function getClient(){
        return $this->twilio;
    }

    public function getRoom($room){
        return $this->twilio->video->v1->rooms($room)->recordings->read([], 20);

        $roomSid = "RM4470ee5c00e6766bbf05aba741ffb3a4";

        $recordingSid = "RTd7f9e338551c46fbfc16f1f2bb67361f";

        $uri = "https://video.twilio.com/v1/" .
            "Rooms/$roomSid/" .
            "Recordings/$recordingSid/" .
            "Media/";

        $response = $this->twilio->request("GET", $uri);

        $mediaLocation = $response->getContent()["redirect_to"];

        $media_content = file_get_contents($mediaLocation);

        file_put_contents("prueba.mkv", $media_content);

    }


    public function createComposition($room, $participant){
        return $this->twilio->video->compositions->create($room, [
            'audioSources' => '*',
            'videoLayout' =>  array(
                'single' => array (
                    'video_sources' => array($participant)
                )
            ),
            'statusCallback' => 'https://datardev.789.com.mx//callbacks',
            'format' => 'mp4'
        ]);

    }

    public function getParticipant($room, $identity){
        $sid = null;

        $participants =  $this->twilio->video->rooms($room)
            ->participants->read(array("status" => "disconnected"));

        foreach ($participants as $participant){
            if ($participant->identity == $identity){
                $sid = $participant->sid;
                break;
            }
       }

        if ($sid){
            return $sid;
        }

        $participants =  $this->twilio->video->rooms($room)
            ->participants->read(array("status" => "connected"));

        foreach ($participants as $participant){
            if ($participant->identity == $identity){
                $sid = $participant->sid;
                break;
            }
        }

        return $sid;
    }

    public function downloadMediaFile($compositionSid){

        $uri = "https://video.twilio.com/v1/Compositions/$compositionSid/Media?Ttl=3600";

        $response = $this->twilio->request("GET", $uri);

        return $response->getContent()["redirect_to"];

        //return $mediaLocation;

        //$name = Str::random('10').".mp4";

        //file_put_contents($name, fopen($mediaLocation, 'r'));
    }

}
