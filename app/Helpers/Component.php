<?php

namespace App\Helpers;

class Component
{
    /**
     * Render a paginator component
     * @param int $paginationSize
     * @param int $pivote
     * @param int $totalPages
     * @param array $params
     * @return view
     */
    public static function pagination($paginationSize, $pivote, $totalPages, $params)
    {
        $size = $paginationSize;
        $limit = floor($size / 2);
        $isFirst = ($pivote == 1);
        $isLast = ($pivote == $totalPages || $totalPages == 0);
        $paginate = [];
        $start = 0;
        $params = empty($params) ? [] : $params;
        if ($totalPages === 1) {
            return '';
        }
        $params['page'] = 1;
        // Two items to help in pagination: First | <<
        $paginate[] = [
            'text' => 'First',
            'href' => $isFirst ? '' : '?' . make_url_params($params)
        ];
        $params['page'] = $pivote - 1;
        $paginate[] =[
            'text' => '&laquo;', // <<
            'href' =>  $isFirst ? '' : '?' . make_url_params($params)
        ];
        // Items are more than limit to show.
        if (($pivote + $limit) >  $totalPages) {
            $start = $totalPages - $size;
        }
        else if (($pivote - $limit) < 1) {
            $start = 1;
        }
        else {
            $start = $pivote -$limit;
        }
        // Items are less than total you want to show.
        if ($totalPages <= $size) {
            $start = 1;
            $end = $totalPages;
        }
        else {
            $end = $start + $size;
        }
        // Create items with number of page.
        for ($i = $start; $i <= $end ; $i++) {
            $params['page'] = $i;
            $paginate[] = [
                'text' => $i,
                'active' => ($i == $pivote) ? 'active' : '',
                'href' => '?' . make_url_params($params)
            ];
        }
        // Two items to help in pagination: >> | Last
        $params['page'] = $pivote + 1;
        $paginate[] = [ 
            'text' =>'&raquo;', // >>
            'href' => $isLast ? '' : '?' . make_url_params($params)
        ];
        $params['page'] = $totalPages;
        $paginate[] = [
            'text' => 'Last',
            'href' => $isLast ? '' : '?' . make_url_params($params)
        ];
        // Render
        return view('components.pagination')->with(['paginate' => $paginate]);
    }
}