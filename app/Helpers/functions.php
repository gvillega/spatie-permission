<?php

use Spatie\Permission\Models\Permission;

function hasPermission($permissions, $slug) {
    foreach ($permissions as $permission) {
        $true = Permission::where('id', $permission->permission_id)->first();

        if (isset($true) && $true->name === $slug) {
            return true;
        }
    }
    return false;
}

function make_url_params(array $params)
{
    $url_params = '';
    $count = count($params);
    $i = 1;
    foreach ($params as $key => $value) {
        $url_params .= "$key=$value";
        if ($i < $count) {
            $url_params .= "&";
        }
        $i++;
    }
    return $url_params;
}
