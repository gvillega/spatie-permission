<?php


namespace App\Helpers;


use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use mastani\GoogleStaticMap\Format;
use mastani\GoogleStaticMap\GoogleStaticMap;
use mastani\GoogleStaticMap\MapType;
use mastani\GoogleStaticMap\Size;

class GoogleImage
{
    public static function download($latitude, $longitude, $path, $zoom = 15){
        $key = config('services.googlemaps.key');
        try {
            $map = new GoogleStaticMap($key);
            $url = $map->setMapType(MapType::RoadMap)
                ->setZoom(15)
                ->setSize(600, 300)
                ->setFormat(Format::JPG)
                ->addMarkerLatLng($latitude, $longitude, '', 'red', Size::Large)
                ->download($path);
            return $url;
        }catch (\Exception $exception){
            Log::info($exception->getMessage());
            return '';
        }
    }

    public static function getAddressForCoordinatesn($latitude, $longitude){
        if (!$latitude && !$longitude) return null;
        try {
            $client = new \GuzzleHttp\Client();
            $geocoder = new \Spatie\Geocoder\Geocoder($client);
            $geocoder->setApiKey(config('geocoder.key'));
            $geocoder->setCountry(config('geocoder.country', 'MX'));
            $data = $geocoder->getAddressForCoordinates($latitude, $longitude);
            return $data["formatted_address"];
        }catch (\Exception $e){
            return '';
        }
    }
}
