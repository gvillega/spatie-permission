<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class RolController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        
        // Checar permiso
        $this->checkPermission($user, 'roles');
    }
}
