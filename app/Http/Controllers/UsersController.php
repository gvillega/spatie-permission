<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    public function index()
    {

    }

    public function create()
    {
        
    }

    public function store()
    {
        
    }

    public function show()
    {
        
    }

    public function edit($id)
    {
        $user = User::find($id);

        $permission = Permission::all();

        return view('user.edit', compact('user', 'permission'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->permissions()->detach();
        $permissions = $request->get('permissions', []);
       
        foreach ($permissions as $slug => $val) { 
            $permission = Permission::find($val);
            if (!empty($permission)) {
                $user->permissions()->attach($permission->id);
            }
        }

        return redirect()->route('home');
    }
}
