<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Exceptions\AccessDeniedHttpException;
use Spatie\Permission\Models\Permission;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function checkPermission($user, $slugPermission)
    {

        $permission = null;
        $name = null;
        if (!hasPermission($user->getPermission(), $slugPermission)) {
            $permission = Permission::where('name', $slugPermission)->first();
            $name = !empty($permission) ? $permission->name : $slugPermission;
            throw new AccessDeniedHttpException(trans('responses.messages.not_permission', ['permission' => $name]));
        }
        return true;
    }
}
